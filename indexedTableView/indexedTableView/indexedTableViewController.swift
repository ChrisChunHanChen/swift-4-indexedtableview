//
//  indexedTableViewController.swift
//  indexedTableView
//
//  Created by Chris Chen on 9/2/16.
//  Copyright © 2016 ChrisDoCoding. All rights reserved.
//

import UIKit

class indexedTableViewController: UITableViewController {
    
    var medicalDictionary: NSDictionary!
    var medicalSort = [String]()
    var medicalList: NSMutableArray = NSMutableArray()
    var medicalSearchResult: NSMutableArray = NSMutableArray()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let path = Bundle.main.path(forResource: "MedicalName", ofType: "plist")
        medicalDictionary = NSDictionary(contentsOfFile: path!)
        medicalSort = (medicalDictionary.allKeys as! [String]).sorted(by:<)
        
        for key in medicalSort {
            let medicalArray = medicalDictionary[key] as! NSArray
            for name in medicalArray {
                medicalList.add(name)
            }
        }

        self.navigationItem.title = "Medical List"
        
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if medicalSearchResult.count != 0 {
            return 1
        }
        return medicalSort.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if medicalSearchResult.count != 0 {
            return medicalSearchResult.count
        }
        let medicals = medicalDictionary!.object(forKey: medicalSort[section])
        return (medicals! as AnyObject).count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if medicalSearchResult.count != 0 {
            return nil
        }
        return medicalSort[section]
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicalCell", for: indexPath)

        // Configure the cell...
        if medicalSearchResult.count != 0 {
            cell.textLabel!.text = medicalSearchResult[(indexPath as NSIndexPath).row] as? String
            
            return cell
        }
        let medicals: NSArray = medicalDictionary!.object(forKey: medicalSort[(indexPath as NSIndexPath).section]) as! NSArray

        cell.textLabel!.text = medicals[(indexPath as NSIndexPath).row] as? String
        
        return cell
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return medicalSort
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return medicalSort.index(of: title)!
    }
    
    func filterContentForSearchText(_ searchText: String) {
        medicalSearchResult.removeAllObjects()
        if searchText.characters.count > 0  {
            let lowercasedArray = medicalList.map { ($0 as AnyObject).lowercased} as [String]

            let filtered = lowercasedArray.filter { ($0 as AnyObject).contains(searchText.lowercased()) }
            
            medicalSearchResult.addObjects(from: filtered.map { ($0 as AnyObject).capitalized})
            
            print("here is the result :", medicalSearchResult)

        }
        tableView.reloadData()
    }
    
    
//    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
//        filteredCandies = candies.filter({( candy : Candy) -> Bool in
//            let categoryMatch = (scope == "All") || (candy.category == scope)
//            return categoryMatch && candy.name.lowercased().contains(searchText.lowercased())
//        })
//        tableView.reloadData()
//    }
    
    //pragma mark Search Display Delegate Methods
//  
//    - (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//    {
//    [filteredNames removeAllObjects];
//    if (searchString.length > 0)
//    {
//    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSString *name, NSDictionary *b)
//    {   NSRange range = [name rangeOfString:searchString options:NSCaseInsensitiveSearch];
//    return range.location != NSNotFound;}];
//    for (NSString *key in self.keys)
//    {
//    NSArray *matches = [self.names[key] filteredArrayUsingPredicate: predicate];
//    [filteredNames addObjectsFromArray:matches];
//    }
//    }
//    return YES;
//    }
    
//
//    
//    
//    // MARK: - Search Bar Delegate
//    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
//        searchBar .resignFirstResponder()
//    }

    
    
//    func filterContentForSearchText(searchText: String, scope: String = "All") {
//        filteredCandies = candies.filter({( candy : Candy) -> Bool in
//            let categoryMatch = (scope == "All") || (candy.category == scope)
//            return categoryMatch && candy.name.lowercaseString.containsString(searchText.lowercaseString)
//        })
//        tableView.reloadData()
//    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension indexedTableViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
//    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
//        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
//    }
}

extension indexedTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
//        let searchBar = searchController.searchBar
//        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
